"""UTILS for main function"""
import glob
import math
import numpy as np
import matplotlib.pyplot as plt

from scipy import ndimage
from typing import overload
from imageio import imread

# constants
SQUARE_WIDTH = 20
PADDING = 2
TEMPLATE_WIDTH = SQUARE_WIDTH + PADDING * 2

PLOT_SETUP = True
PLOT_ITERATION = False

"""If Using Custom image input values here"""
USING_CUSTOM_IMAGE = True
CUSTOM_IMAGE_PATH = "checkerboard_test.png"
CUSTOM_IMAGE_TEMPLATE_PATH = "checkerboard_template.png"


def rgb2gray(img):
    """
    Converts an RGB image into a greyscale image

    Inputs: 
    -img: ndarray of an RGB image of shape (H x W x 3)

    Returns: 
    -bw: ndarray of the corresponding grayscale image of shape (H x W)

    """

    if img.ndim != 3 or img.shape[-1] != 3:
        print(
            f"Invalid image! Please provide an RGB image of the shape (H x W x 3) instead. Current Shape: {img.shape}"
        )
        return None

    return np.dot(img[..., :3], [0.2989, 0.5870, 0.1140])


def compute_gradients(img):
    """
    Computes the gradients of the input image in the x and y direction using a
    differentiation filter.

    Inputs: 
    - img: grayscale image of shape (H x W)
    
    Returns a tuple of: 
    - gx: the gradient in x
    - gy: the gradient in y
    """
    Mx = np.array([[-1, 0, 1]]) / 2
    My = Mx.T

    gy = ndimage.correlate(img, My, np.float64, "constant", 0)
    gx = ndimage.correlate(img, Mx, np.float64, "constant", 0)

    return gx, gy


def compute_homography(t1, t2):
    """
    Computes the Homography matrix for corresponding image points t1 and t2

    The function should take a list of N ≥ 4 pairs of corresponding points
    from the two views, where each point is specified with its 2d image
    coordinates.

    Inputs:
    - t1: Nx2 matrix of image points from view 1
    - t2: Nx2 matrix of image points from view 2

    Returns:
    - H: 3x3 Homography matrix
    """

    #############################################################################
    # TODO: Compute the Homography matrix H for corresponding image points t1, t2
    #############################################################################
    if len(t1.shape) == 2:
        ones = np.ones((t1.shape[0], 1))
        t1 = np.concatenate((t1, ones), axis=1)
        t2 = np.concatenate((t2, ones), axis=1)

    L = np.empty((t1.shape[0] * 2, 9))
    for index, point in enumerate(t1):
        negUp = -t2[index][0] * point
        negVp = -t2[index][1] * point
        L[index * 2] = np.concatenate([point, [0, 0, 0], negUp])
        L[index * 2 + 1] = np.concatenate([[0, 0, 0], point, negVp])

    LTL = np.dot(L.T, L)
    w, v = np.linalg.eig(LTL)
    minEigIndex = np.where(w == w.min())
    H = v[:, minEigIndex[0][0]]
    H = H.reshape(3, 3)
    H = H / H[2, 2]

    return H


def pick_points(img1):
    """
    Functionality to get manually identified corresponding points from two views.

    Inputs:
    - img1: The first image to select points from

    Output:
    - coords1: An ndarray of shape (N, 2) with points from image 1
    """
    ############################################################################
    # TODO: Implement pick_points
    ##########################################
    coords1 = []

    def onClick(event):
        x, y = event.xdata, event.ydata
        if event.inaxes == ax1:
            print(x, y)
            ax1.scatter(x, y, color="b")
            fig.canvas.draw()
            coords1.append((x, y))

    fig, (ax1) = plt.subplots(
        nrows=1, ncols=1, figsize=(20, 10), sharex=False, sharey=False
    )
    ax1.imshow(img1)
    ax1.axis("off")
    # ax1.set_xlim(900, 930)
    # ax1.set_ylim(550, 510)
    ax1.set_title("Image 1", fontsize=10)

    cid = fig.canvas.mpl_connect("button_press_event", onClick)

    plt.show()
    fig.canvas.mpl_disconnect(cid)

    return np.array(coords1)


def params_to_mat(p):
    """
    Take a list p = (p0, p1, p2, p3, p4, p5, p6, p7) and convert it to a homography

    Inputs:
    - p: a size 8 array of paramters

    Returns:
    - h: 3x3 homography matrix
    """
    mat = np.array(
        [
            [p[0] + 1, p[1]    , p[2]],
            [p[3]    , p[4] + 1, p[5]],
            [p[6]    , p[7]    ,    1],
        ]
    )

    return mat


def incr_warp(p, delta_p):
    """
    Takes a list of p = (p0, p1, p2, p3, p4, p5, p6, p7) and delta_p = (dp0, dp1, dp2, dp3, dp4, dp5, dp6, dp7)
    and performs an incremental warp of the params of `p` with the params of `delta_p`

    Inputs:
    - p: an array of length 8 of p homography params
    - delta_p: an array of length 8 of p param deltas

    Returns:
    - p: a new list of `p` params transformed with `delta_p`
    """
    p0, p1, p2, p3, p4, p5, p6, p7 = p
    dp0, dp1, dp2, dp3, dp4, dp5, dp6, dp7 = delta_p

    det = (
        dp5 * p7
        + dp0 * dp5 * p7
        - dp2 * dp3 * p7
        + p6 * (dp2 - dp1 * dp5 + dp2 * dp4)
        - dp0
        + dp1 * dp3
        - dp4
        - dp0 * dp4
        - 1
    )

    t1 = (
        (dp3 * p1 - dp5 * dp6 * p1) + p2 * (dp6 - dp3 * dp7 + dp6 * dp4) + (p0 + 1) * (dp5 * dp7 - dp4 - 1)
    ) / det - 1
    t2 = (
        dp7 * p2
        + dp0 * dp7 * p2
        - dp1 * dp6 * p2
        - p1
        - dp0 * p1
        + dp2 * dp6 * p1
        + dp1
        - dp2 * dp7
        + dp1 * p0
        - dp2 * dp7 * p0
    ) / det
    t3 = (
        dp5 * p1
        + dp0 * dp5 * p1
        - dp2 * dp3 * p1
        + (p0 + 1) * (dp2 - dp1 * dp5 + dp2 * dp4)
        + p2 * (-dp0 + dp1 * dp3 - dp4 - dp0 * dp4 - 1)
    ) / det
    t4 = (
        p5 * (dp6 - dp3 * dp7 + dp6 * dp4)
        + dp3
        - dp5 * dp6
        + dp3 * p4
        - dp5 * dp6 * p4
        + p3 * (dp5 * dp7 - dp4 - 1)
    ) / det
    t5 = (
        dp1 * p3
        - dp2 * dp7 * p3
        + dp7 * p5
        + dp0 * dp7 * p5
        - dp1 * dp6 * p5
        - dp0
        + dp2 * dp6
        - p4
        - dp0 * p4
        + dp2 * dp6 * p4
        - 1
    ) / det - 1
    t6 = (
        p3 * (dp2 - dp1 * dp5 + dp2 * dp4)
        + dp5
        + dp0 * dp5
        - dp2 * dp3
        + dp5 * p4
        + dp0 * dp5 * p4
        - dp2 * dp3 * p4
        + p5 * (-dp0 + dp1 * dp3 - dp4 - dp0 * dp4 - 1)
    ) / det
    t7 = (dp3 * p7 - dp5 * dp6 * p7 + dp6 - dp3 * dp7 + dp6 * dp4 + p6 * (dp5 * dp7- dp4 - 1)) / det
    t8 = (dp7 + dp0 * dp7 - dp1 * dp6 + dp1 * p6 - dp2 * dp7 * p6 - p7 - dp0 * p7 + dp2 * dp6 * p7) / det

    return np.array([t1, t2, t3, t4, t5, t6, t7, t8])


def jacobian(img, p):
    """Returns the jacobian of a homography given the points x and y

    Inputs:
    - h: a 3x3 homography matrix
    - x: the x component of a point
    - y: the y component of a point
    """
    y, x = np.indices(img.shape)

    h = params_to_mat(p)
    D = (h[2, 0] * x) + (h[2, 1] * y) + 1

    xp = h[0, 0] * x + h[0, 1] * y + h[0, 2] / D
    yp = h[1, 0] * x + h[1, 1] * y + h[1, 2] / D

    J = np.zeros((x.shape[0], x.shape[1], 2, 8))

    z = np.zeros_like(x)
    o = np.ones_like(x)

    J = np.moveaxis(
        np.array(
            [[x, y, o, z, z, z, -xp * x, -xp * y], [z, z, z, x, y, o, -yp * x, -yp * y]]
        ),
        [0, 1, 2, 3],
        [2, 3, 0, 1],
    )

    return J[:, :] / D[:, :, np.newaxis, np.newaxis]


def steepest_descent_image(t_gx, t_gy, J_mat):

    t_gx = np.repeat(t_gx[:, :, np.newaxis], 8, axis=2)
    t_gy = np.repeat(t_gy[:, :, np.newaxis], 8, axis=2)
    steepest_descent = np.multiply(t_gx, J_mat[:, :, 0]) + np.multiply(
        t_gy, J_mat[:, :, 1]
    )
    return steepest_descent


def hessian(stdescent):
    """
    Calculate the hessian given the steepest descent image

    Inputs:
    - stdescent: the steepest descent image

    Returns:
    - hessian: the hessian matrix
    """
    stdescent = np.expand_dims(stdescent, axis=2)
    stdescent_t = np.transpose(stdescent, axes=(0, 1, 3, 2))

    return np.sum(stdescent_t[:, :] @ stdescent[:, :], axis=(0, 1))


def apply_homography_xy(xs, ys, H=np.identity(3)):
    """
    Apply a homography transformation with a list of xs and a list of ys

    Inputs:
    - xs: a list of x coordinates
    - ys: a list of y coordinates
    - H: the homography, defaults to the identity transform

    Returns:
    - pts: an Nx2 numpy list of points that have been warped by the homography.
           they are in the same order as the points passed to the function
    """
    if xs.shape != ys.shape:
        print(f"Error! Shape of Xs and Ys does not match!: {xs.shape} != {ys.shape}")
        return None

    ones = np.ones_like(xs)
    points = np.array([xs, ys, ones])
    points = np.expand_dims(np.moveaxis(points, 0, -1), axis=2)
    homogeneous_points = H @ points[:]

    xs = homogeneous_points[:, 0, 0] / homogeneous_points[:, 2, 0]
    ys = homogeneous_points[:, 1, 0] / homogeneous_points[:, 2, 0]
    
    return np.array([xs, ys]).T


def apply_homography_pts(pts, H=np.identity(3)):
    """
    Apply a homography transformation to an Nx2 list of x, y coordinates

    Inputs:
    - pts: an Nx2 numpy list of points
    - H: the homography, defaults to the identity transform

    Returns:
    - pts: an Nx2 numpy list of points that have been warped by the homography.
           they are in the same order as the points passed to the function
    """
    if pts.shape[1] != 2 and pts.ndim != 2:
        print(f"Error! Matrix of points must be of Nx2 format : {pts.shape}")
        return None

    xs = pts[:, 0]
    ys = pts[:, 1]

    return apply_homography_xy(xs, ys, H)

def rectify(img, H=np.identity(3), template=None):
    """
    Warps a reference image into the template space. This method performs the same action as `rectification`
    but vectorizes all operations.

    Inputs:
    - img: input image
    - H: 3x3 homography matrix
    - template: the template image into which you are warping the reference image, if none is provided, 
                the dimensions of the template are assumed to be the same as the image

    Returns:
    - new_img: The input image warped bounded to template space
    """
    if template is None:
        height, width = img.shape
        new_img = np.zeros((height, width))
        cols, rows = np.indices((height, width))
        ones = np.ones((height, width))
    else:
        height, width = template.shape
        new_img = np.zeros((height, width))
        cols, rows = np.indices((height, width))
        ones = np.ones((height, width))

    points = np.array([rows, cols, ones])
    points = np.expand_dims(np.moveaxis(points, 0, -1), axis=3)

    homogeneous_points = np.zeros((height, width, 3, 1))
    homogeneous_points[:, :] = np.linalg.inv(H) @ points[:, :]

    euclidean_points = np.zeros((height, width, 2))
    euclidean_points[:, :, 0] = (
        homogeneous_points[:, :, 1, 0] / homogeneous_points[:, :, 2, 0]
    )
    euclidean_points[:, :, 1] = (
        homogeneous_points[:, :, 0, 0] / homogeneous_points[:, :, 2, 0]
    )
    euclidean_points = euclidean_points.astype(np.int)

    bitmask = np.zeros((height, width), dtype=bool)
    bitmask[:, :] = (
        (euclidean_points[:, :, 0] < img.shape[0]) & 
        (euclidean_points[:, :, 0] >= 0)
    ) & (
        (euclidean_points[:, :, 1] < img.shape[1]) & 
        (euclidean_points[:, :, 1] >= 0)
    )

    bitmask2d = np.repeat(bitmask[:, :, np.newaxis], 2, axis=2)

    (tmp,) = euclidean_points[bitmask2d].shape
    valids = euclidean_points[bitmask2d].reshape((tmp // 2, 2))

    new_img[cols[bitmask], rows[bitmask]] = img[valids[:, 0], valids[:, 1]]

    return new_img


def rectification(input_image, H, template=None):
    """
    Warps and merges an input image onto the reference image.

    Inputs:
    - input_image: input image
    - H: 3x3 homography matrix
    - input_point: 4 points from input image checker corners

    Returns a tuple of:
    - warp_image: The input image warped according to H.
    - output: The input image warped bounded to boundry of template
    """

    if template is not None:
        template = rgb2gray(imread(template))
        output = np.zeros_like(template)
    else:
        output = np.ones((TEMPLATE_WIDTH, TEMPLATE_WIDTH)) * 255

    H_inv = np.linalg.inv(H)

    for row in range(output.shape[0]):
        for col in range(output.shape[1]):
            # shifting starting point to min values
            point = np.asarray([col, row, 1])
            original_point = np.dot(H_inv, point)
            y = original_point[0] / original_point[2]
            x = original_point[1] / original_point[2]
            if (
                x < input_image.shape[0]
                and y < input_image.shape[1]
                and x >= 0
                and y >= 0
            ):
                interpolation = input_image[math.floor(x), math.floor(y)]
                output[row, col] = interpolation
    return output

#!/bin/env python

"""
CS4476 Final Project: Computes Inverse Compositional Algorithm for Checkerboard Corner Detection
"""

import os
import glob
import math
import time
import tqdm
import argparse
import numpy as np
import matplotlib.pyplot as plt

from utils import *
from imageio import imread, imsave

parser = argparse.ArgumentParser(description="Run the Inverse Compositional Warp Algorithm")
parser.add_argument("image", metavar="I", type=str, nargs=1, help="The image to match against the template")
parser.add_argument("-t", type=str, dest="template", help="Use custom template image")
parser.add_argument("-m", dest="max_iterations", type=int, default=100, help="Maximum number of iterations (default = 100)")
parser.add_argument("-p", dest="pick_points", action="store_true", help="Pick points manually instead of loading from file")
parser.add_argument("-s", dest="plot_setup", action="store_true", help="Show setup plots")
parser.add_argument("-i", dest="plot_iterate", action="store_true", help="Show iterative process plots")
parser.add_argument("-v", dest="verbose", action="store_true", help="Enable verbose output mode")
parser.add_argument("--save_iterations", dest="save_iterations", action="store_true", help="Save the iterative transformations of the image")
parser.add_argument("--save_plot_iterations", dest="save_plot_iterations", action="store_true", help="Save the iterative transformations of the image plots")

args = parser.parse_args()

square_points = np.array([[58, 58], [298, 58], [300, 300], [59, 300]])
# square_points = np.array([[34, 32], [132, 35], [133, 130], [35, 131]])

# Initialize by getting first frame, starting corners
# and creating template and storing corners
if args.template is not None:
    img = rgb2gray(imread(args.image[0]))
    if len(img.shape) > 2:
        raise Exception(
            "Image must be grayscale. You can modify the line above to convert it to grayscale"
        )

    starting_corners = np.array(
        [[0, 0], [img.shape[1], 0], [img.shape[1], img.shape[0]], [0, img.shape[0]]]
    )

    if args.pick_points:
        starting_corners = pick_points(img)
    else:
        starting_corners = np.load("test_starting_corners.npy")
    # np.save("test_starting_corners.npy", starting_corners)

    if args.verbose:
        print(starting_corners)

    template = rgb2gray(imread(args.template))

    print(np.max(template))
    print(np.max(img))

    if len(template.shape) > 2:
        raise Exception(
            "Template must be grayscale. You can modify the line above to convert it to grayscale"
        )

    template_corners = np.array(
        [
            [0                    , 0                    ],
            [template.shape[1] - 1, 0                    ],
            [template.shape[1] - 1, template.shape[0] - 1],
            [0                    , template.shape[0] - 1],
        ]
    )
    if args.verbose:
        print(template_corners)

    boundingbox = np.zeros_like(template)

else:
    # create template
    template = np.zeros((TEMPLATE_WIDTH, TEMPLATE_WIDTH))
    template[PADDING : TEMPLATE_WIDTH - PADDING, 0:PADDING] = 255
    template[PADDING : TEMPLATE_WIDTH - PADDING, TEMPLATE_WIDTH - PADDING :] = 255
    template[0:PADDING, PADDING : TEMPLATE_WIDTH - PADDING] = 255
    template[TEMPLATE_WIDTH - PADDING :, PADDING : TEMPLATE_WIDTH - PADDING] = 255

    # template corners
    template_corners = np.array(
        [
            [PADDING, PADDING],
            [TEMPLATE_WIDTH - PADDING, PADDING],
            [TEMPLATE_WIDTH - PADDING, TEMPLATE_WIDTH - PADDING],
            [PADDING, TEMPLATE_WIDTH - PADDING],
        ]
    )
    if args.verbose:
        print(template_corners)
    # template = rgb2gray(imread(args.template))

    if len(template.shape) > 2:
        raise Exception(
            "Template must be grayscale. You can modify the line above to convert it to grayscale"
        )

    template_corners = np.array(
        [
            [0, 0],
            [template.shape[0], 0],
            [template.shape[0], template.shape[1]],
            [0, template.shape[1]],
        ]
    )
    # get first frame
    renders = sorted(glob.glob("Bar/" + "*.jpg"))
    img = rgb2gray(imread(renders[0]))

    """Uncomment to select starting corners get starting corners"""
    starting_corners = pick_points(img)
    np.save("starting_square_corners.npy", starting_corners)

    starting_corners = np.load("starting_square_corners.npy")
    boundingbox = template[
        PADDING : TEMPLATE_WIDTH - PADDING, PADDING : TEMPLATE_WIDTH - PADDING
    ]
    boundingbox = template

# compute gradients
if args.verbose:
    print("Computing Gradients")

t_gx, t_gy = compute_gradients(template)

if args.plot_setup:
    fig, ax = plt.subplots(1, 1)
    ax.set_title("First Frame")
    # starting_corners = np.load("starting_square_corners.npy")

    ax.imshow(img, cmap="gray")
    ax.scatter(starting_corners[:, 0], starting_corners[:, 1])
    plt.show()

    fig, ax = plt.subplots(1, 1)
    ax.set_title("Template")
    ax.imshow(template, cmap="gray")
    plt.show()

    fig2, (ax1, ax2) = plt.subplots(nrows=1, ncols=2, figsize=(5, 5))
    fig2.suptitle("Gradients")
    ax1.set_title("Gx")
    ax1.imshow(t_gx, cmap="gray")

    ax2.set_title("Gy")
    ax2.imshow(t_gy, cmap="gray")

    plt.show()

homography = compute_homography(starting_corners, template_corners)
p = homography.flatten()[:-1] - np.array([1, 0, 0, 0, 1, 0, 0, 0])
homography = params_to_mat(p)
# p = np.zeros(8)
# homography = np.identity(3)

# Precompute the jacobian at every point in the template image with p = 0
if args.verbose:
    print("Computing Jacobian")

J_mat = jacobian(template, p)

# calculate steepest gradient descent images
if args.verbose:
    print("Computing SDI")

stdescent = steepest_descent_image(t_gx, t_gy, J_mat)

# calculate hessian and hessian inverse
if args.verbose:
    print("Computing Hessian")
H = hessian(stdescent)
H_inv = np.linalg.pinv(H)

if args.template is None and args.verbose:
    homography_inv_test = np.linalg.inv(homography)
    transformed_corners = apply_homography_pts(template_corners, homography_inv_test)

    for idx in range(len(template_corners)):
        print(f"{template_corners[idx]} --> {transformed_corners[idx]}")


# print("Iterating")
ITERATION = 0
# i = 0
delta = np.inf
DELTA_THRESHOLD = 0.00001
MAX_ITERS = 100

start_iter_timer = time.time()

t = tqdm.tqdm(range(args.max_iterations), ncols=180)
for i in t:
    output = rectify(img, homography, template)
    
    # Compute error image
    difference = template - output

    # Compute steepest descent parameter updates
    b = np.sum(stdescent[:, :, :] * difference[:, :, np.newaxis], axis=(0, 1))

    # Calculate gradient descent paramter updates
    deltaP = H_inv @ b

    # Update warp parameters
    p = incr_warp(p, deltaP)
    homography = params_to_mat(p)
    homography_inv_test = np.linalg.inv(homography)

    # Calculate error
    diff_error = math.sqrt(np.sum(difference * difference))
    delta = sum(deltaP ** 2)

    t.set_postfix({"delta_p": delta, "ssd": diff_error})

    if args.save_plot_iterations:
        # homography_inv = np.linalg.inv(homography)
        # transformed_corners = apply_homography_pts(square_points, homography_inv)

        fig, ((ax, ax1), (ax2, ax3)) = plt.subplots(2, 2, figsize=(10, 10))
        ax.set_title("input image")
        ax.imshow(img, cmap="gray")
        # ax.scatter(transformed_corners[:, 0], transformed_corners[:, 1])
        ax.axis("off")

        ax1.set_title("template")
        ax1.imshow(template, cmap="gray")
        # ax1.scatter(square_points[:, 0], square_points[:, 1])
        ax1.axis("off")

        ax2.set_title(f"output at {i}")
        ax2.imshow(output.astype(np.uint8), cmap="gray")
        ax2.axis("off")

        ax3.set_title("difference")
        ax3.imshow(difference.astype(np.uint8), cmap="gray")
        ax3.axis("off")
        # plt.show()

        if args.save_plot_iterations:
            if not os.path.exists(f"Warps/Plots/{args.image[0][:-4]}"):
                os.makedirs(f"Warps/Plots/{args.image[0][:-4]}")
        
        # imsave(f"Warps/Test/{args.image[0][:-4]}/frame_{i}.png", output.astype(np.uint8))
        plt.savefig(f"Warps/Plots/{args.image[0][:-4]}/frame_{i}.png")
        plt.close()

homography_inv = np.linalg.inv(homography)
if args.verbose:
    print("Original Corners:")
    print(template_corners)
    print("Template Corners x Inverse Homography:")
    print(homography)

transformed_corners = apply_homography_pts(square_points, homography_inv)

if args.verbose:
    for idx in range(len(template_corners)):
        print(f"{template_corners[idx]} --> {transformed_corners[idx]}")

if args.verbose:
    print("Starting Corners:")
    print(starting_corners)

# plot final results
fig, ((ax, ax1), (ax2, ax3)) = plt.subplots(2, 2, figsize=(10, 10))
ax.set_title("input")
ax.imshow(img.astype(np.uint8), cmap="gray")
ax.scatter(transformed_corners[:, 0], transformed_corners[:, 1])

ax1.set_title("template")
ax1.imshow(template.astype(np.uint8), cmap="gray")
ax1.scatter(square_points[:, 0], square_points[:, 1])

ax2.set_title("output")
ax2.imshow(output.astype(np.uint8), cmap="gray")

ax3.set_title("difference")
ax3.imshow(difference.astype(np.uint8), cmap="gray")
plt.show()

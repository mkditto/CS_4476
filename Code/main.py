#!/bin/env python
"CS4476 Final Project: Computes Inverse Compositional Algorithm for Checkerboard Corner Detection"
import glob
import math
import time
import numpy as np
import matplotlib.pyplot as plt
from imageio import imread, imsave
from utils import *


# Initialize by getting first frame, starting corners
# and creating template and storing corners
if USING_CUSTOM_IMAGE:
    img = rgb2gray(imread(CUSTOM_IMAGE_PATH))
    if len(img.shape) > 2:
        raise Exception(
            "Image must be grayscale. You can modify the line above to convert it to grayscale"
        )

    starting_corners = np.array(
        [[0, 0], [img.shape[1], 0], [img.shape[1], img.shape[0]], [0, img.shape[0]]]
    )
    # starting_corners = pick_points(img)
    # np.save("test_starting_corners.npy", starting_corners)
    starting_corners = np.load("test_starting_corners.npy")
    print(starting_corners)

    template = rgb2gray(imread(CUSTOM_IMAGE_TEMPLATE_PATH))

    if len(template.shape) > 2:
        raise Exception(
            "Template must be grayscale. You can modify the line above to convert it to grayscale"
        )

    template_corners = np.array(
        [
            [0, 0],
            [template.shape[1], 0],
            [template.shape[1], template.shape[0]],
            [0, template.shape[0]],
        ]
    )
    print(template_corners)

    boundingbox = np.zeros((template.shape[0], template.shape[1]))

else:
    # create template
    template = np.zeros((TEMPLATE_WIDTH, TEMPLATE_WIDTH))
    template[PADDING : TEMPLATE_WIDTH - PADDING, 0:PADDING] = 255
    template[PADDING : TEMPLATE_WIDTH - PADDING, TEMPLATE_WIDTH - PADDING :] = 255
    template[0:PADDING, PADDING : TEMPLATE_WIDTH - PADDING] = 255
    template[TEMPLATE_WIDTH - PADDING :, PADDING : TEMPLATE_WIDTH - PADDING] = 255

    # template corners
    template_corners = np.array(
        [
            [PADDING, PADDING],
            [TEMPLATE_WIDTH - PADDING, PADDING],
            [TEMPLATE_WIDTH - PADDING, TEMPLATE_WIDTH - PADDING],
            [PADDING, TEMPLATE_WIDTH - PADDING],
        ]
    )
    print(template_corners)
    template = rgb2gray(imread(CUSTOM_IMAGE_TEMPLATE_PATH))

    if len(template.shape) > 2:
        raise Exception(
            "Template must be grayscale. You can modify the line above to convert it to grayscale"
        )

    template_corners = np.array(
        [
            [0, 0],
            [template.shape[0], 0],
            [template.shape[0], template.shape[1]],
            [0, template.shape[1]],
        ]
    )
    # get first frame
    renders = sorted(glob.glob("Bar/" + "*.jpg"))
    img = rgb2gray(imread(renders[0]))

    """Uncomment to select starting corners get starting corners"""
    starting_corners = pick_points(img)
    np.save("starting_square_corners.npy", starting_corners)

    starting_corners = np.load("starting_square_corners.npy")
    boundingbox = template[
        PADDING : TEMPLATE_WIDTH - PADDING, PADDING : TEMPLATE_WIDTH - PADDING
    ]
    boundingbox = template

# compute gradients
print("Computing Gradients")
t_gx, t_gy = compute_gradients(template)

if PLOT_SETUP:
    fig, ax = plt.subplots(1, 1)
    ax.set_title("First Frame")
    # starting_corners = np.load("starting_square_corners.npy")

    ax.imshow(img, cmap="gray")
    ax.scatter(starting_corners[:, 0], starting_corners[:, 1])
    plt.show()

    fig, ax = plt.subplots(1, 1)
    ax.set_title("Template")
    ax.imshow(template, cmap="gray")
    plt.show()

    fig2, (ax1, ax2) = plt.subplots(nrows=1, ncols=2, figsize=(5, 5))
    fig2.suptitle("Gradients")
    ax1.set_title("Gx")
    ax1.imshow(t_gx, cmap="gray")

    ax2.set_title("Gy")
    ax2.imshow(t_gy, cmap="gray")

    plt.show()

# Compute starting homography based on starting points
# if USING_CUSTOM_IMAGE:
# p = np.zeros((8))
# else:
homography = compute_homography(starting_corners, template_corners)
p = homography.flatten()[:-1]
p[0] -= 1
p[4] -= 1
homography = params_to_mat(p)

# Precompute the jacobian at every point in the template image with p = 0
print("Computing Jacobian")
cols, rows = np.indices(template.shape)
J_mat = jacobian(rows, cols, p)

# calculate steepest gradient descent images
print("Computing SDI")
stdescent = steepest_descent_image(t_gx, t_gy, J_mat)
print(stdescent.shape)
# calculate hessian and hessian inverse
print("Computing Hessian")
H = hessian(stdescent)
H_inv = np.linalg.pinv(H)

# initialize iteration params
print("Initializing Homography")

if not USING_CUSTOM_IMAGE:
    homography_inv_test = np.linalg.inv(homography)
    for corner in template_corners:
        corner = [corner[0], corner[1], 1]
        point = homography_inv_test @ corner
        new_corner = [point[0] / point[2], point[1] / point[2]]
        print(f"{corner} --> {new_corner}")
print("Iterating")
ITERATION = 0
# i = 0
delta = np.inf
DELTA_THRESHOLD = 0.00001
MAX_ITERS = 100

start_iter_timer = time.time()
while delta > DELTA_THRESHOLD and ITERATION < 100:
    ITERATION += 1

    # compute warped image with current parameters
    # if USING_CUSTOM_IMAGE:
    # output = apply_homography(img, homography)
    # else:
    output = rectification(img, homography)
    imsave(f"Warps/Test/checker_test_{ITERATION}.png", output)
    # compute error image
    difference = template - output

    # compute steepest descent parameter updates
    b = np.sum(stdescent[:, :, :] * difference[:, :, np.newaxis], axis=(0, 1))

    # if not USING_CUSTOM_IMAGE:
    #     # compute error image
    #     difference = boundingbox - output

    #     # compute steepest descent parameter updates
    #     for x in range(boundingbox.shape[0]):
    #         for y in range(boundingbox.shape[1]):
    #             x_idx = x + PADDING
    #             y_idx = y + PADDING
    #             b += stdescent[x_idx, y_idx] * difference[x, y]

    # compute gradient descent parameter updates
    deltaP = H_inv @ b

    # update warp parameters
    p = incr_warp(p, deltaP)
    homography = params_to_mat(p)

    homography_inv_test = np.linalg.inv(homography)

    # calculate error
    diff_error = math.sqrt(np.sum(difference * difference))
    delta = sum(deltaP ** 2)

    # plot results from current iteration
    # imsave(f"Warps/Test/Test{i}.png", warp_image.astype(np.uint8))
    # i += 1

    if PLOT_ITERATION:
        fig, ((ax, ax1), (ax2, ax3)) = plt.subplots(2, 2)
        ax.set_title("input image")
        ax.imshow(img, cmap="gray")

        ax1.set_title("template")
        ax1.imshow(template, cmap="gray")

        ax2.set_title("test warp")
        ax2.imshow(output.astype(np.uint8), cmap="gray")

        ax3.set_title("difference")
        ax3.imshow(difference.astype(np.uint8), cmap="gray")
        plt.show()
end_iter_timer = time.time()
print(
    f"Finished {ITERATION} iterations in {end_iter_timer - start_iter_timer} seconds."
)
if ITERATION < MAX_ITERS:
    print(f"Converged in {ITERATION} iterations")

homography_inv = np.linalg.inv(homography)
print("Original Corners:")
print(template_corners)
print("Template Corners x Inverse Homography:")
print(homography)
for corner in template_corners:
    corner = [corner[0], corner[1], 1]
    point = homography_inv @ corner
    new_corner = [point[0] / point[2], point[1] / point[2]]
    print(f"{corner} --> {new_corner}")

print("Starting Corners:")
print(starting_corners)


# plot final results
fig, ((ax, ax1), (ax2, ax3)) = plt.subplots(2, 2, figsize=(10, 10))
ax.set_title("input")
ax.imshow(img.astype(np.uint8), cmap="gray")

ax1.set_title("template")
ax1.imshow(template.astype(np.uint8), cmap="gray")

ax2.set_title("output")
ax2.imshow(output.astype(np.uint8), cmap="gray")

ax3.set_title("difference")
ax3.imshow(difference.astype(np.uint8), cmap="gray")
plt.show()

import cv2
from glob import glob
import numpy as np
from tqdm import tqdm
import os
import sys

CHECKERBOARD = (24, 24)
criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 5, 0.001)
if len(sys.argv) != 2:
    sys.exit("Please input a file name as an argument")
folder = sys.argv[1]
print(f"Processing images from {folder}")
cameras = [
    sorted(glob(f"../{folder}/" + "Camera_1*.jpg")),
    sorted(glob(f"../{folder}/" + "Camera_2*.jpg")),
]
height, width, layers = cv2.imread(cameras[0][0]).shape
size = (width, height)
for index, camera in enumerate(cameras):
    imgs = []
    for fname in tqdm(camera, desc=f"Processing Camera {index + 1}"):
        img = cv2.imread(fname)
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        # Find the chess board corners
        # If desired number of corners are found in the image then ret = true
        ret, corners = cv2.findChessboardCorners(
            gray,
            CHECKERBOARD,
            cv2.CALIB_CB_ADAPTIVE_THRESH
            + cv2.CALIB_CB_FAST_CHECK
            + cv2.CALIB_CB_NORMALIZE_IMAGE,
        )
        if ret == True:
            # refining pixel coordinates for given 2d points.
            corners2 = cv2.cornerSubPix(gray, corners, (11, 11), (-1, -1), criteria)
            img = cv2.drawChessboardCorners(img, CHECKERBOARD, corners2, ret)
        fname = fname[4 + len(folder) :]
        imgs.append(img)
        # cv2.imwrite(path, img)
        # cv2.imshow(fname, img)
        # cv2.waitKey(10)
    path = f"CV_Detections/Open_Cv_{folder}_Camera_{index + 1}.mp4"
    out = cv2.VideoWriter(path, cv2.VideoWriter_fourcc(*"mp4v"), 24, size)
    print(f"Saving {path}")
    for i in imgs:
        # writing to a image array
        out.write(i)
    out.release()
print("Done")
cv2.destroyAllWindows()

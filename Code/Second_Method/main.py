import os
import sys
import argparse
import numpy as np
from glob import glob
from imageio import imread
from pick_points import pick_points

GRID_COL = 25
GRID_ROW = 25
WS_MIN = 11
WS_MAX = 21
# Pick points
renders = sorted(glob('../Bar/' + "*.jpg"))
img = imread(renders[0])

# checkerboard_boundary = pick_points(img, "Pick 4 boundry points")
# np.save("checkerboard_boundary.npy", checkboard_boundary)
checkerboard_boundary = np.load("./checkerboard_boundary.npy")
max_x = max(checkerboard_boundary[:, 0])
min_x = min(checkerboard_boundary[:, 0])
max_y = max(checkerboard_boundary[:, 1])
min_y = min(checkerboard_boundary[:, 1])
width = max_x - min_x
height = max_y - min_y

g_width = int(min(width/GRID_COL, height/GRID_ROW))
w_prime = g_width
if g_width < WS_MIN:
    w_prime = WS_MIN
elif g_width > WS_MAX:
    w_prime = WS_MAX

w = round(w_prime / 2)
p = checkerboard_boundary[0] + w
L = [][]
for r in range(0, w + 1):
    L[0]
